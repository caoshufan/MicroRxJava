package me.caoshufan.microrxjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.caoshufan.microrxjava.function.BiFunction;
import me.caoshufan.microrxjava.function.Function;
import me.caoshufan.microrxjava.observable.Observable;
import me.caoshufan.microrxjava.observable.ObservableEmitter;
import me.caoshufan.microrxjava.observable.ObservableOnSubscribe;
import me.caoshufan.microrxjava.observable.Observer;
import me.caoshufan.microrxjava.observable.RLog;
import me.caoshufan.microrxjava.scheduler.AndroidSchedulers;
import me.caoshufan.microrxjava.scheduler.Scheduler;
import me.caoshufan.microrxjava.scheduler.Schedulers;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = findViewById(R.id.test);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click();
            }
        });


    }

    private void click(){
        //
//        Observable.create(new ObservableOnSubscribe<Integer>() {
//            @Override
//            public void subscribe(ObservableEmitter<Integer> observableEmitter) throws Exception {
//                RLog.printInfo("emitter发送第一个onNext，value = 1");
//                observableEmitter.onNext(1);
//                RLog.printInfo("emitter发送第二个onNext，value = 2");
//                observableEmitter.onNext(2);
//                RLog.printInfo("emitter发送onComplete");
//                observableEmitter.onComplete();
//            }
//        }).flatMapIterable(new Function<Integer, Iterable<String>>() {
//            @Override
//            public Iterable<String> apple(Integer value) throws Exception {
//                List<String> list = new ArrayList<>();
//                list.add("aaa" + value);
//                list.add("bbb" + value);
//                return list;
////                String[] str = new String[2];
////                str[0] = "aaa" + value;
////                str[1] = "bbb" + value;
////                return str;
//            }
//        }).subscribe(new Observer<String>() {
//            @Override
//            public void onSubscribe() {
//                RLog.printInfo("subscribe");
//            }
//
//            @Override
//            public void onComplete() {
//                RLog.printInfo("complete");
//            }
//
//            @Override
//            public void onError(Throwable throwable) {
//                throwable.printStackTrace();
//            }
//
//            @Override
//            public void onNext(String value) {
//                RLog.printInfo("observer :" + value);
//            }
//        });


        Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> observableEmitter) throws Exception {
                RLog.printInfo("emitter发送第一个onNext，value = 1");
                observableEmitter.onNext(1);
                RLog.printInfo("emitter发送第二个onNext，value = 2");
                observableEmitter.onNext(2);
                RLog.printInfo("emitter发送onComplete");
                observableEmitter.onComplete();
            }
        }).map(new Function<Integer, String>() {
            @Override
            public String apple(Integer value) throws Exception {
                return "!!!" + value;
            }
        }).subscribe(new Observer<String>() {
            @Override
            public void onSubscribe() {
                RLog.printInfo("subscribe");
            }

            @Override
            public void onComplete() {
                RLog.printInfo("complete");
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onNext(String value) {
                RLog.printInfo("observer :" + value);
            }
        });
//
//        Observable<Integer> observable1 = Observable.create(new ObservableOnSubscribe<Integer>() {
//            @Override
//            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
//                RLog.printInfo("subscribe: observable1 emitter发送第一个onNext，value = 1");
//                emitter.onNext(1);
//                RLog.printInfo("subscribe: observable1 emitter发送第二个onNext，value = 2");
//                emitter.onNext(2);
//                RLog.printInfo("subscribe :observable1 emitter发送onComplete");
//                emitter.onComplete();
//            }
//        });
//
//        Observable<String> observable2 = Observable.create(new ObservableOnSubscribe<String>() {
//            @Override
//            public void subscribe(ObservableEmitter<String> emitter) throws Exception {
//                RLog.printInfo("subscribe: observable2 emitter发送第一个onNext，value = A");
//                emitter.onNext("A");
//                RLog.printInfo("subscribe :observable2 emitter发送第二个onNext，value = B");
//                emitter.onNext("B");
//                //RLog.printInfo("observable2 emitter发送onComplete");
//                //emitter.onComplete();
//            }
//        });
//
//        Observable.zip(observable1, observable2, new BiFunction<Integer, String, String>() {
//            @Override
//            public String apply(Integer integer, String s) throws Exception {
//                RLog.printInfo("apply : " + integer+s);
//                return integer + s;
//            }
//        }).subscribe(new Observer<String>() {
//            @Override
//            public void onSubscribe() {
//                RLog.printInfo("Observer被订阅");
//            }
//
//            @Override
//            public void onNext(String value) {
//                RLog.printInfo("Observer接收到onNext，被Zip转换之后的value = " + value);
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                RLog.printInfo("Observer接收到onError，errorMsg = " + e.getMessage());
//            }
//
//            @Override
//            public void onComplete() {
//                RLog.printInfo("Observer接收到onComplete");
//            }
//        });
    }
}

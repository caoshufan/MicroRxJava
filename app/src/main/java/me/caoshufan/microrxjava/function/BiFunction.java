package me.caoshufan.microrxjava.function;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/26
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface BiFunction<T1, T2, R> {
    R apply(T1 t1,T2 t2) throws Exception;
}

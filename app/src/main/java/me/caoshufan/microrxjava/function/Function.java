package me.caoshufan.microrxjava.function;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/20
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface Function<T,R> {
    R apple(T value) throws Exception;
}

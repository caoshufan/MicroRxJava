package me.caoshufan.microrxjava.observable;

import me.caoshufan.microrxjava.scheduler.Scheduler;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/30
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class ObservableSubscribeOn<T> extends Observable<T> {

    final ObservableSource<T> source;
    final Scheduler scheduler;

    public ObservableSubscribeOn(ObservableSource<T> source, Scheduler scheduler){
        RLog.printInfo("ObservableSubscribeOn");
         this.scheduler  = scheduler;
         this.source = source;
    }

    @Override
    public void subscribeActual(Observer<? super T> observer) {
        RLog.printInfo("ObservableSubscribeOn subscribeActual");
        final SubscribeOnObserver<T> subscribeOnObserver = new SubscribeOnObserver<>(observer);
        scheduler.scheduleDirect(new Scheduler.Worker() {
            @Override
            protected void execute() {
                RLog.printInfo("我在这里切换");
                source.subscribe(subscribeOnObserver);
            }
        });
    }


    static final class SubscribeOnObserver<T> implements Observer<T> {

        final Observer<? super T> observer;

        public SubscribeOnObserver(Observer<? super T> observer) {
            RLog.printInfo("SubscribeOnObserver");
            this.observer = observer;
        }

        @Override
        public void onSubscribe() {
            RLog.printInfo("SubscribeOnObserver onSubscribe");
            observer.onSubscribe();
        }

        @Override
        public void onComplete() {
            RLog.printInfo("SubscribeOnObserver onComplete");
            observer.onComplete();
        }

        @Override
        public void onError(Throwable throwable) {
            RLog.printInfo("SubscribeOnObserver onError");
            observer.onError(throwable);
        }

        @Override
        public void onNext(T value) {
            RLog.printInfo("SubscribeOnObserver onNext");
            observer.onNext(value);
        }
    }
}

package me.caoshufan.microrxjava.observable;

import java.util.Iterator;

import me.caoshufan.microrxjava.function.Function;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/25
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class ObservableFlapMapIterable<T,U> extends Observable<U> {


    final ObservableSource<T> source;
    final Function<? super T,Iterable<U>> mapper;

    public ObservableFlapMapIterable(ObservableSource<T> source, Function<? super T, Iterable<U>> mapper){
        this.source = source;
        this.mapper = mapper;
    }

    public ObservableSource<T> getSource() {
        return source;
    }


    @Override
    public void subscribeActual(Observer<? super U> observer) {
        MapperObserver mapperObserver = new MapperObserver(observer,mapper);
        source.subscribe(mapperObserver);
    }


    static final class MapperObserver<T,U> implements Observer<T> {

        final Observer<? super U> observer;
        final Function<? super T , Iterable<U>> function;

        public MapperObserver(Observer<? super U> observer, Function<? super T,Iterable<U>> function){
            this.observer = observer;
            this.function = function;
        }


        @Override
        public void onSubscribe() {
            observer.onSubscribe();
        }

        @Override
        public void onComplete() {
            observer.onComplete();
        }

        @Override
        public void onError(Throwable throwable) {
            observer.onError(throwable);
        }

        @Override
        public void onNext(T value) {
            RLog.printInfo("ObservableFlapMapSimple: onNext");
            Iterable<? extends U>  p = null;
            try{
                p = function.apple(value);
            }catch (Exception e) {
                observer.onError(e);
                e.printStackTrace();
            }
            Iterator<? extends U> it = p.iterator();
            while (it.hasNext()) {
                observer.onNext(it.next());
            }
        }

    }
}

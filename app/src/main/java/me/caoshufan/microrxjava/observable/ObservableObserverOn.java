package me.caoshufan.microrxjava.observable;

import me.caoshufan.microrxjava.scheduler.Scheduler;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/30
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class ObservableObserverOn<T> extends Observable<T> {

    private final ObservableSource<T> source;
    private final Scheduler scheduler;

    public ObservableObserverOn(ObservableSource<T> source,Scheduler scheduler){
        RLog.printInfo("ObservableObserverOn");
        this.scheduler = scheduler;
        this.source = source;
    }

    @Override
    public void subscribeActual(Observer<? super T> observer) {
        RLog.printInfo("ObservableObserverOn subscribeActual");
        SchedulerObserver<T> schedulerObserver = new SchedulerObserver<>(observer,scheduler);
        source.subscribe(schedulerObserver);
    }


    final static class SchedulerObserver<T> implements Observer<T>{

        final Scheduler scheduler;
        final Observer<? super T> observer;


        public SchedulerObserver(Observer<? super T> observer,Scheduler scheduler) {
            RLog.printInfo("SchedulerObserver");
            this.observer = observer;
            this.scheduler = scheduler;
        }

        @Override
        public void onSubscribe() {
            RLog.printInfo("SchedulerObserver onSubscribe");
            scheduler.scheduleDirect(new Scheduler.Worker() {
                @Override
                protected void execute() {
                    observer.onSubscribe();
                }
            });
        }

        @Override
        public void onComplete() {
            RLog.printInfo("SchedulerObserver onComplete");
            scheduler.scheduleDirect(new Scheduler.Worker() {
                @Override
                protected void execute() {
                    observer.onComplete();
                }
            });
        }

        @Override
        public void onError(final Throwable throwable) {
            RLog.printInfo("SchedulerObserver onError");
            scheduler.scheduleDirect(new Scheduler.Worker() {
                @Override
                protected void execute() {
                    observer.onError(throwable);
                }
            });
        }

        @Override
        public void onNext(final T value) {
            RLog.printInfo("SchedulerObserver onNext");
            scheduler.scheduleDirect(new Scheduler.Worker() {
                @Override
                protected void execute() {
                    observer.onNext(value);
                }
            });
        }
    }


}

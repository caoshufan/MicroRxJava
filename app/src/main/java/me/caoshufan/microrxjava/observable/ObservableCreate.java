package me.caoshufan.microrxjava.observable;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/19
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class ObservableCreate<T> extends Observable<T> {

    final ObservableOnSubscribe<T> source;

    public ObservableCreate (ObservableOnSubscribe<T> observableOnSubscribe){
        RLog.printInfo("ObservableCreate");
        source = observableOnSubscribe;
    }


    @Override
    public void subscribeActual(Observer<? super T> observer) {
        RLog.printInfo("ObservableCreate subscribeActual");
        CreateEmitter<T> createEmitter = new CreateEmitter<>(observer);
        observer.onSubscribe();
        try {
            RLog.printInfo("emitter开始发送事件");
            source.subscribe(createEmitter);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }


    static final class CreateEmitter<T> implements ObservableEmitter<T> {

        final Observer<? super T> observer;

        CreateEmitter(Observer<? super T> observer){
            RLog.printInfo("CreateEmitter");
            this.observer = observer;
        }

        @Override
        public ObservableEmitter<T> serialize() {
            return null;
        }

        @Override
        public void onNext(T value) {
            RLog.printInfo("CreateEmitter onNext");
            observer.onNext(value);
        }

        @Override
        public void onError(Throwable throwable) {
            RLog.printInfo("CreateEmitter onError");
            observer.onError(throwable);
        }

        @Override
        public void onComplete() {
            RLog.printInfo("CreateEmitter onComplete");
            observer.onComplete();
        }
    }
}

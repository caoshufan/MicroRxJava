package me.caoshufan.microrxjava.observable;


import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import me.caoshufan.microrxjava.function.BiFunction;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/26
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class ObservableZip<T,R> extends Observable<R> {

    BiFunction<? super Object, ? super Object, R> biFunction;
    final ObservableSource<? extends T>[] sources;

    public ObservableZip(ObservableSource<? extends T>[] sources,
                         BiFunction<? super Object, ? super Object,R> biFunction) {

        this.biFunction = biFunction;
        this.sources = sources;

    }


    @Override
    public void subscribeActual(Observer<? super R> observer) {
        RLog.printInfo("Zip subscribeActual");

        ZipCoordinator<T,R> zc = new ZipCoordinator<>(observer,sources,biFunction);
        zc.subscribe();
    }

    static final class ZipCoordinator<T,R> {
        final Observer<? super R> observer;
        final ObservableSource<? extends T>[] sources;
        final BiFunction<? super Object, ? super Object , R> biFunction;
        final ZipObserver<T,R>[] observers;
        final T[] row;

        ZipCoordinator(Observer<? super R> observer,ObservableSource<? extends T>[] sources,
                       BiFunction<? super Object,? super Object, R> biFunction) {
            this.observer = observer;
            this.sources = sources;
            this.biFunction = biFunction;
            this.observers = new ZipObserver[sources.length];
            this.row = (T[]) new Object[sources.length];
        }

        public void subscribe() {
            int len = observers.length;
            for (int i = 0; i < len; i++) {
                observers[i] = new ZipObserver<>(this);
            }
            observer.onSubscribe();
            for (int i = 0; i  < len; i++) {
                sources[i].subscribe(observers[i]);
            }
        }

        public void drain(){
            final T[] os = row;
            outer:
            while (true) {
                int length = observers.length;
                for (int i = 0; i < length ; i++) {
                    ZipObserver<T,R> zipObserver = observers[i];
                    Queue<T> queue = zipObserver.queue;
                    if (queue.isEmpty()) {
                        if (observers[i].done) {
                             observer.onComplete();
                        }
                        break outer;
                    }
                    if (i == 1) {
                        os[0] = observers[0].queue.poll();
                        os[1] = observers[1].queue.poll();
                        if (null != os[0] && null != os[1]) {
                            try {
                                R result = biFunction.apply(os[0],os[1]);
                                observer.onNext(result);
                                Arrays.fill(os,null);
                            }catch (Exception e) {
                                e.printStackTrace();
                                observer.onError(e);
                            }
                        }
                    }
                }
            }
        }


    }


    static final class ZipObserver<T,R> implements Observer<T> {

        final ZipCoordinator<T,R> parent;
        final Queue<T> queue = new LinkedBlockingQueue<>();
        volatile boolean done;

        ZipObserver(ZipCoordinator<T,R> parent) {
            this.parent = parent;
        }

        @Override
        public void onSubscribe() {

        }

        @Override
        public void onComplete() {
            done = true;
            parent.drain();
        }

        @Override
        public void onError(Throwable throwable) {
            parent.drain();
        }

        @Override
        public void onNext(T value) {
            queue.offer(value);
            parent.drain();
        }
    }


}

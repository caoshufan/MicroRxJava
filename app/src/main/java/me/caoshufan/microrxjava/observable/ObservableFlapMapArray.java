package me.caoshufan.microrxjava.observable;

import me.caoshufan.microrxjava.function.Function;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/25
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class ObservableFlapMapArray<T,U> extends Observable<U> {

    final ObservableSource<T> source;

    final Function<? super T, U[]> mapper;

    public ObservableFlapMapArray(ObservableSource<T> source, Function<? super T,U[]> mapper) {
        this.source = source;
        this.mapper = mapper;
    }


    @Override
    public void subscribeActual(Observer<? super U> observer) {
        MapArrayObserver mapArrayObserver = new MapArrayObserver(observer,mapper);
        source.subscribe(mapArrayObserver);
    }


    static final class MapArrayObserver<T,U> implements Observer<T>{

        final Observer<? super U> observer;
        final Function<? super T,U[]> mapper;

        public MapArrayObserver(Observer<? super U> observer, Function<? super T,U[]> mapper) {
            this.observer = observer;
            this.mapper = mapper;
        }


        @Override
        public void onSubscribe() {
            observer.onSubscribe();
        }

        @Override
        public void onComplete() {
            observer.onComplete();
        }

        @Override
        public void onError(Throwable throwable) {
            observer.onError(throwable);
        }

        @Override
        public void onNext(T value) {
            U[] p = null;
            try{
                p = mapper.apple(value);
            }catch (Exception e) {
                observer.onError(e);
                e.printStackTrace();
            }
            if (p != null)
                for (U item : p) {
                    observer.onNext(item);
                }
        }

    }
}

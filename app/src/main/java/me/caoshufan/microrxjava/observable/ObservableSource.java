package me.caoshufan.microrxjava.observable;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/19
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface ObservableSource<T> {

    void subscribe(Observer< ? super T> observer);
}

package me.caoshufan.microrxjava.observable;

import me.caoshufan.microrxjava.function.BiFunction;
import me.caoshufan.microrxjava.function.Function;
import me.caoshufan.microrxjava.scheduler.Scheduler;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/19
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public abstract class Observable<T> implements ObservableSource<T>{


    public static <T> Observable<T> create(ObservableOnSubscribe<T> observableOnSubscribe) {
        RLog.printInfo("Observable create");
        return new ObservableCreate<>(observableOnSubscribe);
    }

    public abstract void subscribeActual(Observer<? super T> observer);

    @Override
    public void subscribe(Observer<? super T> observer) {
        RLog.printInfo("Observable subscribe");
        subscribeActual(observer);
    }

    public final Observable<T> observerOn(Scheduler scheduler){
        RLog.printInfo("Observable observerOn");
        return new ObservableObserverOn<>(this,scheduler);
    }

    public final Observable<T> subscribeOn(Scheduler scheduler) {
        RLog.printInfo("Observable subscribeOn");
        return new ObservableSubscribeOn<>(this,scheduler);
    }

    public final <R> Observable<R> map(Function<? super T,? extends R> mapper){
        RLog.printInfo("Observable map");
        return new ObservableMap<>(this,mapper);
    }

    public final <R> Observable<R> flatMapIterable(Function<? super T , Iterable<R>> mapper) {
        return new ObservableFlapMapIterable<>(this,mapper);
    }

    public final <R> Observable<R> flatMapArray(Function<? super T, R[]> mapper) {
        return new ObservableFlapMapArray<>(this,mapper);
    }

    public static <T1,T2,R> Observable<R> zip(ObservableSource<? extends T1> source1, ObservableSource<? extends T2> source2,
                                              BiFunction<? super T1,? super T2, R> zipper){
        return zipArrayBiFunction((BiFunction<? super Object ,? super Object,R>) zipper ,source1,source2);
    }
    public static <T,R> Observable<R> zipArrayBiFunction(BiFunction<? super Object,? super Object, R> biFunction,ObservableSource<? extends T> ... sources) {
        return new ObservableZip<T,R>(sources,biFunction);
    }
}

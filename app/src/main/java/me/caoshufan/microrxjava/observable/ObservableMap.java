package me.caoshufan.microrxjava.observable;

import me.caoshufan.microrxjava.R;
import me.caoshufan.microrxjava.function.Function;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/20
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class ObservableMap<T,U> extends Observable<U>{

    private final Function<? super T,? extends U> mapper;
    private final ObservableSource<T> source;

    public ObservableMap(ObservableSource<T> source, Function<? super T,? extends U> mapper){
        RLog.printInfo("ObservableMap");
        this.mapper = mapper;
        this.source = source;
    }

    public final ObservableSource<T> source(){
        return source;
    }

    @Override
    public void subscribeActual(Observer<? super U> observer) {
        RLog.printInfo("map subscribeActual");
        final MapObserver<T,U> mapObserver = new MapObserver<>(observer,mapper);
        source.subscribe(mapObserver);
    }

    static final class MapObserver<T,U> implements Observer<T> {

        final Observer<? super U> observer;
        final Function<? super T,? extends U> mapper;

        public MapObserver(Observer<? super U> observer,Function<? super T,? extends U> mapper){
            RLog.printInfo("MapObserver");
            this.observer = observer;
            this.mapper = mapper;
        }

        @Override
        public void onSubscribe() {
            RLog.printInfo("map subscribe");
            observer.onSubscribe();
        }

        @Override
        public void onComplete() {
            observer.onComplete();
        }

        @Override
        public void onError(Throwable throwable) {
            observer.onError(throwable);
        }

        @Override
        public void onNext(final T value) {
            RLog.printInfo("map onNext");
            U v = null;
            try {
                v = mapper.apple(value);
            }catch (Exception e) {
                e.printStackTrace();
            }
            observer.onNext(v);
        }
    }
}

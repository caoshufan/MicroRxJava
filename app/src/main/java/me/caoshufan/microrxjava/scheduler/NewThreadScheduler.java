package me.caoshufan.microrxjava.scheduler;

import android.support.annotation.NonNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/30
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class NewThreadScheduler extends Scheduler{

    private static final  String TAG = "NewThreadScheduler";

    public static NewThreadScheduler getInstance(){
        return NewThreadSchedulerHolder.INSTANCE;
    }

    private static class NewThreadSchedulerHolder{
        private static NewThreadScheduler INSTANCE = new NewThreadScheduler();
    }

    @Override
    public void scheduleDirect(Runnable runnable) {
        executorService().submit(runnable);
    }

    private static ExecutorService executorService;

    private static synchronized ExecutorService executorService() {
        if (executorService == null) {
            executorService = new ThreadPoolExecutor(0,Integer.MAX_VALUE,60, TimeUnit.SECONDS,new SynchronousQueue<Runnable>(),threadFactory(false));
        }
        return executorService;
    }

    private static ThreadFactory threadFactory(final boolean daemon){
        final AtomicInteger mCount = new AtomicInteger(1);
        return new ThreadFactory() {
            @Override
            public Thread newThread(@NonNull Runnable runnable) {
                Thread thread = new Thread(runnable,"RxJava New Thread #" +mCount.getAndIncrement());
                thread.setDaemon(daemon);
                return thread;
            }
        };

    }
}

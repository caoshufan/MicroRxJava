package me.caoshufan.microrxjava.scheduler;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/30
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public final class Schedulers {
    public static final Scheduler IO = IOSchedulers.getInstance();
    public static final Scheduler NEW_THREAD = NewThreadScheduler.getInstance();
    public static final Scheduler ANDROID_MAIN_THREAD = AndroidSchedulers.getInstance();

}

package me.caoshufan.microrxjava.scheduler;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/30
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public abstract class Scheduler {
    public abstract void scheduleDirect(Runnable runnable);

    public static abstract class Worker extends NamedRunnable{

    }

    public static abstract class NamedRunnable implements Runnable{

        @Override
        public void run() {
            execute();
        }

        protected  abstract void execute();
    }
}

package me.caoshufan.microrxjava.scheduler;

import android.support.annotation.NonNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * ProjectName:Knowledge_Android_V1
 * Description:
 * Created by Devin on 2018/7/30
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class IOSchedulers extends Scheduler {
    private static final String TAG = "NewThreadScheduler";
    private static final int PRIORITY = 5;
    public static IOSchedulers getInstance() {
        return IOSchedulerHolder.INSTANCE;
    }

    private static class IOSchedulerHolder{
        private static IOSchedulers INSTANCE = new IOSchedulers();
    }

    @Override
    public void scheduleDirect(Runnable runnable) {
        executorService().submit(runnable);
    }

    private static ExecutorService executorService;
    private static synchronized ExecutorService executorService() {
        if (executorService == null) {
            executorService = Executors.newScheduledThreadPool(PRIORITY,new IOThreadFactory(PRIORITY));
        }
        return executorService;
    }

    private static class IOThreadFactory implements ThreadFactory {

        final int priority;

        public IOThreadFactory(int priority) {
            this.priority = priority;
        }

        @Override
        public Thread newThread(@NonNull Runnable runnable) {
            final AtomicInteger mCount = new AtomicInteger(1);
            Thread result = new Thread(runnable,"RxJava IO Thread #" + mCount.getAndIncrement());
            result.setPriority(priority);
            result.setDaemon(true);
            return result;
        }
    }
}
